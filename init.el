;;
;; Settings of paths
;;

;; Define a function to add arguments to "load-path".
(defun add-to-load-path (&rest paths)
  (let (path)
    (dolist (path paths paths)
      (let ((default-directory
	      (expand-file-name (concat user-emacs-directory path))))
        (add-to-list 'load-path default-directory)
        (if (fboundp 'normal-top-level-add-subdirs-to-load-path)
            (normal-top-level-add-subdirs-to-load-path))))))

;; Add directories and theirs sub directories to load-path.
(add-to-load-path "elisp" "elpa" "conf" "public_repos")

;;
;; Settings of repositories
;;

;; Settings for package.el.
(when (require 'package nil t)
  ;; Add Marmalade, ELPA and MELPA.
  (add-to-list 'package-archives '("ELPA" . "http://tromey.com/elpa/"))
  (add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/"))
  (add-to-list 'package-archives
               '("marmalade" . "http://marmalade-repo.org/packages/"))
  (package-initialize))

;;
;; Settings of performance
;;

;; Save history of buffers.
(savehist-mode t)

;; Save place of cursor in file.
(setq-default save-place t)
(require 'saveplace)

;; Raise number of lines of log.
(setq message-log-max 10000)

;; Raise number of histories.
(setq history-length 10000)

;; Display key stroke quickly in echo area.
(setq echo-keystrokes 0.1)

;; Display number of columns.
(column-number-mode t)

;; Display size of file.
(size-indication-mode t)

;;
;; Settings of key binds
;;

;; Assign Backspace to C-h.
(keyboard-translate ?\C-h ?\C-?)

;; Assign query-replace to C-c r.
(define-key global-map (kbd "C-c r") 'query-replace)

;; Assign Help to C-x ?.
(define-key global-map (kbd "C-x ?") 'help-command)

;; Assign toggle truncate lines to C-c l.
(define-key global-map (kbd "C-c l") 'toggle-truncate-lines)

;; Assign toggle window to C-t.
(define-key global-map (kbd "C-t") 'other-window)

;; Assign comment-region to C-c c.
(define-key global-map (kbd "C-c c") 'comment-region)

;; Assign uncomment-region to C-c u.
(define-key global-map (kbd "C-c u") 'uncomment-region)

;; Assign align to C-c a.
(define-key global-map (kbd "C-c a") 'align)

;; Assign backward-kill-word to M-h.
(define-key global-map (kbd "M-h") 'backward-kill-word)

;; Assign goto-line to M-g.
(define-key global-map (kbd "M-g") 'goto-line)

;;
;; Settings of built-in functions
;;

;; Revert buffer automatically when file was changed.
(global-auto-revert-mode t)

;; Use only rectangle edit function on cua-mode.
(cua-mode t)
(setq cua-enable-cua-keys nil)

;; Set default encofing UTF-8.
(set-language-environment "Japanese")
(prefer-coding-system 'utf-8)
(when (eq window-system 'w64)
  (set-file-name-coding-system 'cp932)
  (setq locale-coding-system 'cp932))

;; Save backup and auto-save files in "~/.emacs.d/backups/".
(add-to-list 'backup-directory-alist
             (cons "." "~/.emacs.d/backups"))
(setq auto-save-file-name-transforms
      `((".*" ,(expand-file-name "~/.emacs.d/backups") t)))

;; Turn off KKC.
(global-set-key (kbd "C-\\") 'nil)

;; Change yes/no to y/n.
(fset 'yes-or-no-p 'y-or-n-p)

;; Ignore cases of a character.
(setq completion-ignore-case t)

;; Change the identification string of files that have same name.
(when (require 'uniquify nil t)
  (setq uniquify-buffer-name-style 'post-forward-angle-brackets))

;; Delete trailing whitespace before save file.
(add-hook 'before-save-hook 'delete-trailing-whitespace)

;; Delete newline character at the end of the line when executing C-k.
(setq kill-whole-line t)

;; Copy directory recursively.
(setq dired-recursive-copies 'always)
;; Match only file name when do I-search in dired buffer.
(setq dired-isearch-filenames t)

;;
;; Settings of display
;;

;; Set ascii font.
(set-face-attribute 'default nil
                    :family "Consolas"
                    :height 100)

;; Set Hiragino Kaku Gothic ProN as Japanese set.
(set-fontset-font
 nil 'japanese-jisx0208
 (font-spec :family "メイリオ"))

(setq default-tab-width 4)

;; Length of time it takes to display highlight of parentheses.
(setq show-paren-delay 0)

(show-paren-mode t)

(global-hl-line-mode t)

;; Highlight expression in parenthesis.
(setq show-paren-style 'expression)

(set-face-background 'show-paren-match-face nil)
(set-face-underline-p 'show-paren-match-face "yellow")

;; Not display tool bar, menu bar and scroll bar except for terminal.
(when window-system
  (tool-bar-mode 0)
  (menu-bar-mode 0)
  (scroll-bar-mode 0))

;; Configure transparence.
(when window-system
  (set-frame-parameter nil 'alpha 90))

;; Show the function name where the cursor is in.
(which-function-mode t)

;;
;; Settings of external functions
;;

;; Redo+
;; (when (require 'redo+ nil t)
;;  (global-set-key (kbd "C-.") 'redo))

;; Helm
;; (when (require 'helm-config nil t)
;;   (helm-mode 1)
;;   (global-set-key (kbd "C-c h") 'helm-mini)
;;   (global-set-key (kbd "M-x") 'helm-M-x))

;;
;; Settings of code style
;;

;; Set the style of C, C++ and Java as "stroustrup".
(defun my-c-mode-hook ()
  (c-set-style "stroustrup")
  (setq indent-tabs-mode nil))
(add-hook 'c-mode-hook 'my-c-mode-hook)
(add-hook 'c++-mode-hook 'my-c-mode-hook)

;; Markdown
;; (add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))

;; COBOL
;; (add-to-list 'auto-mode-alist '("\\.cbl\\'" . cobol-mode))
